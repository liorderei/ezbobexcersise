package com.liorderei;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.liorderei.numbersexercise.GuiNumberExerciseConsole;
import com.liorderei.numbersexercise.contracts.GuiNumberExercise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan("com.liorderei.numbersexercise")
public class Main {

    private static Logger logger = LoggerFactory
            .getLogger(GuiNumberExerciseConsole.class);


    @Bean
    public ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS, MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES);
        return objectMapper;
    }


    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(Main.class);
        GuiNumberExercise guiNumberExerciseConsole = context.getBean(GuiNumberExerciseConsole.class);
        guiNumberExerciseConsole.run();
        logger.info("APPLICATION FINISHED");
    }

}
