package com.liorderei.numbersexercise;

import com.liorderei.numbersexercise.contracts.NumbersService;
import com.liorderei.numbersexercise.contracts.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class NumbersCalculationService implements NumbersService {
    private static Logger logger = LoggerFactory.getLogger(NumbersCalculationService.class);
    private final Map<String, Operation> operators;

    @Autowired
    public NumbersCalculationService(Map<String, Operation> operators) {
        this.operators = operators;
    }

    @Override
    public Integer calculateRequest(NumbersRequest numbersRequest) {

        logger.debug("retrieving operator: {}", numbersRequest.getOperator());
        Operation op = operators.get(numbersRequest.getOperator().getName());

        return op.getOperator().apply(numbersRequest.getNumbers());
    }


}
