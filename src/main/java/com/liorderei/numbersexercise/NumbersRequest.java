package com.liorderei.numbersexercise;

import com.liorderei.numbersexercise.enums.Operators;

import java.util.List;

public class NumbersRequest {

    private Operators operator;

    private List<Integer> numbers;

    public NumbersRequest() {
    }

    public NumbersRequest(Operators op, List<Integer> numbers) {
        this.operator = op;
        this.numbers = numbers;
    }

    public void setNumbers(List<Integer> numbers) {
        this.numbers = numbers;
    }

    public Operators getOperator() {
        return operator;
    }

    public List<Integer> getNumbers() {
        return numbers;
    }
}
