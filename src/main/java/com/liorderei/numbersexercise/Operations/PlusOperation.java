package com.liorderei.numbersexercise.Operations;

import com.liorderei.numbersexercise.contracts.Operation;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;

@Component("plus")
public class PlusOperation implements Operation {
    @Override
    public Function<List<Integer>, Integer> getOperator() {
        return numbers -> numbers.stream().reduce(0, Integer::sum);
    }
}
