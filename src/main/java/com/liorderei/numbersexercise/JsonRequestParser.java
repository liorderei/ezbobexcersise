package com.liorderei.numbersexercise;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.liorderei.numbersexercise.contracts.RequestParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;

@Component
public class JsonRequestParser implements RequestParser {

    private ObjectMapper objectMapper;

    @Autowired
    public JsonRequestParser(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public NumbersRequest buildRequestFromUser(BufferedReader inputReader) throws BadInputException {
        StringBuilder requestBuilder = new StringBuilder();
        String line;
        try {
            //in JDK 9 streams has takeWhile operation which would provide
            // the ability to change this while into stream
            while ((line = inputReader.readLine()) != null) {
                if (line.isEmpty() || line.equals("\n")) {
                    break;
                }
                requestBuilder.append(line);
            }
            return objectMapper.readValue(requestBuilder.toString(), NumbersRequest.class);
        } catch (IOException e) {
            throw new BadInputException("error while reading input from user");
        }
    }


}
