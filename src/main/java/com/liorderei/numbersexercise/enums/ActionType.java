package com.liorderei.numbersexercise.enums;

public enum ActionType {
    EXIT,
    REQUEST
}