package com.liorderei.numbersexercise.enums;


public enum Operators {
    PLUS("plus"),
    MINUS("minus");

    private final String name;


    Operators(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
