package com.liorderei.numbersexercise.contracts;


import java.util.List;
import java.util.function.Function;

public interface Operation {
    Function<List<Integer>, Integer> getOperator();
}
