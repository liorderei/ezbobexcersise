package com.liorderei.numbersexercise.contracts;

import com.liorderei.numbersexercise.BadInputException;
import com.liorderei.numbersexercise.NumbersRequest;

import java.io.BufferedReader;

public interface RequestParser {
    NumbersRequest buildRequestFromUser(BufferedReader inputReader) throws BadInputException;
}
