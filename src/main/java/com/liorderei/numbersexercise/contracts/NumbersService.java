package com.liorderei.numbersexercise.contracts;

import com.liorderei.numbersexercise.BadInputException;
import com.liorderei.numbersexercise.NumbersRequest;

public interface NumbersService {
    Integer calculateRequest(NumbersRequest numbersRequest) throws BadInputException;
}
