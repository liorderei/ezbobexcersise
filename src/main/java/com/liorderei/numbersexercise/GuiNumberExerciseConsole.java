package com.liorderei.numbersexercise;

import com.liorderei.numbersexercise.contracts.GuiNumberExercise;
import com.liorderei.numbersexercise.contracts.NumbersService;
import com.liorderei.numbersexercise.enums.ActionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

@Component
public class GuiNumberExerciseConsole implements GuiNumberExercise {

    private final JsonRequestParser jsonRequestParser;
    private NumbersService numbersService;
    private BufferedReader inputReader;
    private Map<Integer, Runnable> actions;


    @Autowired
    public GuiNumberExerciseConsole(NumbersService numbersService, JsonRequestParser jsonRequestParser) {
        this.numbersService = numbersService;
        this.jsonRequestParser = jsonRequestParser;
        inputReader = new BufferedReader(new InputStreamReader(System.in));
        initActions();
    }

    private void initActions() {
        actions = new HashMap<>();
        actions.put(ActionType.REQUEST.ordinal(), this::requestAction);
        actions.put(ActionType.EXIT.ordinal(), this::exitAction);
    }

    private static Logger logger = LoggerFactory
            .getLogger(GuiNumberExerciseConsole.class);


    @Override
    public void run() {
        logger.info("Welcome to Numbers Exercise");
        int action;
        do {
            printMenu();
            action = getActionNumber();

            actions.getOrDefault(action, () -> logger.warn("wrong input, try again"))
                    .run();

        } while (action != ActionType.EXIT.ordinal());
    }

    private int getActionNumber() {
        try {
            return Integer.parseInt(inputReader.readLine());
        } catch (IOException | NumberFormatException e) {
            logger.error("could not parse action request", e);
        }
        return -1;
    }


    private void requestAction() {
        logger.info("Please provide valid json request:");
        try {
            Integer value;
            NumbersRequest numbersRequest = this.jsonRequestParser.buildRequestFromUser(inputReader);
            value = numbersService.calculateRequest(numbersRequest);
            logger.info("calculation returned {}", value);
        } catch (BadInputException e) {
            logger.error("could not parse request", e);
        }

    }


    private void exitAction() {
        try {
            inputReader.close();
        } catch (IOException e) {
            logger.error("error while closing input reader", e);
        }
    }

    private void printMenu() {
        logger.info("Please choose action:");
        logger.info("0. Exit");
        logger.info("1. Calculate numbers");
    }
}
