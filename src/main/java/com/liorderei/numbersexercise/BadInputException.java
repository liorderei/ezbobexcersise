package com.liorderei.numbersexercise;

public class BadInputException extends Exception {
    BadInputException(String message) {
        super(message);
    }
}
