package com.liorderei.numbersexercise;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.liorderei.numbersexercise.Operations.MinusOperation;
import com.liorderei.numbersexercise.Operations.PlusOperation;
import com.liorderei.numbersexercise.contracts.NumbersService;
import com.liorderei.numbersexercise.contracts.Operation;
import com.liorderei.numbersexercise.enums.Operators;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.HashMap;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class NumberExerciseApplicationTests {
    private ObjectMapper objectMapper;

    private NumbersService numbersService;

    private JsonRequestParser jsonRequestParser;

    @Before
    public void setUp() {

        HashMap<String, Operation> actions = new HashMap<>();
        actions.put(Operators.MINUS.getName(), new MinusOperation());
        actions.put(Operators.PLUS.getName(), new PlusOperation());
        this.objectMapper = initObjectMapper();
        this.jsonRequestParser = new JsonRequestParser(objectMapper);
        this.numbersService = new NumbersCalculationService(actions);
    }

    private ObjectMapper initObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS, MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES);
        return objectMapper;
    }


    @Test
    public void PositiveNumbersWithPlusOperatorTest() throws Exception {
        NumbersRequest request = buildNumberRequest(Operators.PLUS, 1, 2, 3, 4, 5);

        Integer value = numbersService.calculateRequest(request);
        Assert.assertEquals(15, value.intValue());
    }

    @Test
    public void PositiveNumbersWithMinusOperatorTest() throws Exception {
        NumbersRequest request = buildNumberRequest(Operators.MINUS, 30, 15, 5, 3);
        Integer value = numbersService.calculateRequest(request);

        Assert.assertEquals(7, value.intValue());
    }

    @Test
    public void EmptyListWithMinusOperatorTest() throws Exception {
        NumbersRequest request = buildNumberRequest(Operators.MINUS);
        Integer value = numbersService.calculateRequest(request);

        Assert.assertEquals(0, value.intValue());
    }

    @Test
    public void EmptyListNumbersWithMinusOperatorTest() throws Exception {
        NumbersRequest request = buildNumberRequest(Operators.MINUS);
        Integer value = numbersService.calculateRequest(request);

        Assert.assertEquals(0, value.intValue());
    }

    private NumbersRequest buildNumberRequest(Operators plus, Integer... numbers) throws BadInputException {
        ObjectNode requestJson = objectMapper.createObjectNode();

        ArrayNode numbersArrayJson = requestJson.putArray("numbers");
        Arrays.stream(numbers).forEach(numbersArrayJson::add);

        requestJson.put("operator", plus.getName());


        BufferedReader br = new BufferedReader(new StringReader(requestJson.toString()));
        return jsonRequestParser.buildRequestFromUser(br);

    }

}
